<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ahmad Irfan Maulana</title>
    <meta name="description" content="Portfolio Website of Ahmad Irfan Maulana"/>
    <meta name=”robots” content="index, follow">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bowlby+One&family=Manrope&display=swap" rel="stylesheet">

    <style>
        * {
            font-family: 'Montserrat', sans-serif;
        }
        .wrap {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            text-align: center;
            width: 100%;
        }
        body {
            font-family: 'Manrope', sans-serif;
        }
        h1 {
            font-family: 'Bowlby One', cursive;
        }
        .icon {
            font-size: 60px;
            animation-name: floating;
            animation-duration: 5s;
            animation-iteration-count: infinite;
            animation-timing-function: ease-in-out;
        }
        p {
            color: #ccc;
            letter-spacing: 1.3px;
        }
        @keyframes floating {
            0% { transform: translate(0,  0px); }
            50%  { transform: translate(0, -16px); }
            100%   { transform: translate(0, 0px); }
        }
        @media (max-width: 768px) {
            h1 {
                font-size: 24px;
            }
        }
    </style>
</head>
<body>

<div class="wrap">
    <div class="icon">🏗</div>
    <h1>Under Maintenance</h1>
    <p>www.irfanstudio.com</p>
</div>

</body>
</html>